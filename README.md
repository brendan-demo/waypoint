[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/brendan-demo/waypoint)

# Waypoint AWS-ECS Example

|Title|Description|
|---|---|
|Pack|Cloud Native Buildpack|
|Cloud|AWS|
|Language|NodeJS|
|Docs|[AWS-ECS](https://www.waypointproject.io/plugins/aws-ecs)|
|Tutorial|[HashiCorp Learn](https://learn.hashicorp.com/tutorials/waypoint/aws-ecs)|

This example demonstrates the AWS Elastic Container Service `deploy` plugin
which also provides a `build` step for the Elastic Container Registry.
